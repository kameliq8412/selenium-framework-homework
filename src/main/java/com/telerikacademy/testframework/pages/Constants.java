package com.telerikacademy.testframework.pages;

public class Constants {
    public static String ISSUE_DESCRIPTION = "As a client I would like to order a taxi from location to destination " +
            "using taxi application on my phone./nPrerequisites: As a client I have installed the app on my phone, have " +
            "a registration and I am logged in. Steps to reproduce:/n1.Open the app, turn my location on and click " +
            "ok;/n2.Set a destination on the map and click ok;Click button 'Order';3.A notificatin ?A car is traveling" +
            " to your location? is displayed;4.Click confirmation button when the car arrives. 5.Click confirmation" +
            " button when the destination reached.Expected result: An order is created with start and end destination " +
            "point. The order is successfully closed.";


    public static String BUG_DESCRIPTION = "Description: The button ?request a taxi? is not working.\n" +
            "Steps to reproduce:\n" +
            "1.Open the app, turn my location on and click ok;\n" +
            "2.Set a destination on the map and click ok;\n" +
            "3.Click button ?request a taxi?.\n" +
            "Expected result: An order is created with start and end destination point.\n" +
            "Actual result: An order is not created with start and end destination point.";

    public static String BUG_SUMMARY = "The button 'request a taxi?' is not working.";
    public static String STORY_SUMMARY = "As a client I would like to order a taxi.";

    public static String BUG_NAME = "JR3-48";
}
