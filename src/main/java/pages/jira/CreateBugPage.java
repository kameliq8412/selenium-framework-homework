package pages.jira;
import org.openqa.selenium.WebDriver;
import static com.telerikacademy.testframework.pages.Constants.*;

public class CreateBugPage extends BaseJiraPage{

    private String bugDescription;
    private String bugSummary;

    public CreateBugPage(WebDriver driver) {
        super(driver, "jira.projectPage");
    }

    public void clickOnCreateBugButton() {

        actions.waitForElementVisibleUntilTimeout("jira.useCreateIssueButton",60);
        actions.clickElement("jira.useCreateIssueButton");

        actions.waitFor(6000);

        actions.clickElement("jira.useBugTypeBox");

        actions.waitForElementVisibleUntilTimeout("jira.selectBugType", 100);
        actions.clickElement("jira.selectBugType");

        actions.waitForElementVisibleUntilTimeout("jira.useBugSummaryField", 60, bugSummary);
        actions.typeValueInField(BUG_SUMMARY,"jira.useBugSummaryField", bugSummary);

        actions.waitForElementVisibleUntilTimeout("jira.useBugDescriptionField", 60, bugDescription);
        actions.typeValueInField(BUG_DESCRIPTION,"jira.useBugDescriptionField", bugDescription);

        actions.waitForElementVisibleUntilTimeout("jira.useBugPriority", 60);
        actions.clickElement("jira.useBugPriority");

        actions.waitForElementVisibleUntilTimeout("jira.selectBugPriorityType", 60);
        actions.clickElement("jira.selectBugPriorityType");

        actions.clickElement("jira.useCreateBugButton");
    }

    public void assertBugCreated(){
        actions.waitForElementVisibleUntilTimeout("jira.createdMessageButton", 60);
        actions.assertElementPresent("jira.createdMessageButton");
    }
}
