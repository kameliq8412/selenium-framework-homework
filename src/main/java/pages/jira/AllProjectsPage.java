package pages.jira;

import org.openqa.selenium.WebDriver;

public class AllProjectsPage extends BaseJiraPage {

    public AllProjectsPage(WebDriver driver) {
        super(driver, "jira.allProjectsPage");
    }

    public void clickOnProject(String projectName) {
        actions.waitForElementVisibleUntilTimeout("jira.select.jiraSoftwareButton", 60, projectName);
        actions.clickElement("jira.select.jiraSoftwareButton", projectName);
    }
}
