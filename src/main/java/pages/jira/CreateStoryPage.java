package pages.jira;
import org.openqa.selenium.WebDriver;
import static com.telerikacademy.testframework.pages.Constants.*;

public class CreateStoryPage extends BaseJiraPage{

    private String issueDescription;
    private String storySummary;

    public CreateStoryPage(WebDriver driver) {
        super(driver, "jira.projectPage");
    }

    public void clickOnCreateIssueButton() {

        actions.waitForElementVisibleUntilTimeout("jira.useCreateIssueButton", 60);
        actions.clickElement("jira.useCreateIssueButton");

        actions.waitForElementVisibleUntilTimeout("jira.useIssueTypeBox", 60);
        actions.clickElement("jira.useIssueTypeBox");

        actions.waitForElementVisibleUntilTimeout("jira.selectType", 60);
        actions.clickElement("jira.selectType");

        actions.waitForElementVisibleUntilTimeout("jira.useSummaryField", 60, storySummary);
        actions.typeValueInField(STORY_SUMMARY,"jira.useSummaryField", storySummary);

        actions.typeValueInField(ISSUE_DESCRIPTION,"jira.useDescriptionField", issueDescription);

        actions.clickElement("jira.usePriorityDropDown");

        actions.waitForElementVisibleUntilTimeout("jira.selectPriority", 60);
        actions.clickElement("jira.selectPriority");;

        actions.clickElement("jira.useCreateStoryButton");

    }

    public void assertStoryCreated(){
        actions.waitForElementVisibleUntilTimeout("jira.createdMessageButton", 60);
        actions.assertElementPresent("jira.createdMessageButton");
    }
}
