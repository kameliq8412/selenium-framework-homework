package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.Constants.BUG_NAME;

public class LinkStoryAndBug extends BaseJiraPage{

    String bugName;

    public LinkStoryAndBug(WebDriver driver) {
        super(driver, "jira.linkStoryAndBugPage");
    }

    public void linkIssues(){

        actions.waitForElementVisibleUntilTimeout("jira.selectIssue", 60);
        actions.clickElement("jira.linkIssuesButton");

        actions.waitForElementVisibleUntilTimeout("jira.selectIssue", 60);
        actions.clickElement("jira.selectIssue");

        actions.typeValueInField(BUG_NAME,"jira.selectIssueName", bugName);

        actions.clickElement("jira.linkButton");
    }

    public void assertIssuesLiked(){
        actions.waitForElementVisibleUntilTimeout("jira.linkSuccessful", 60);
        actions.assertElementPresent("jira.linkSuccessful");
    }
}
