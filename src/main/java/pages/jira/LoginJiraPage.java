package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginJiraPage extends BaseJiraPage{

    public LoginJiraPage(WebDriver driver) {
        super(driver, "jira.loginUrl");
    }

    public void logInUser(String userKey){
        String username = Utils.getConfigPropertyByKey("jira.users."+ userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.logInPage.username");
        actions.typeValueInField(username, "jira.logInPage.username");
        actions.clickElement("jira.logInPage.loginButton");

        actions.waitFor(6000);

        actions.waitForElementVisible("jira.logInPage.password");
        actions.typeValueInField(password, "jira.logInPage.password");
        actions.clickElement("jira.logInPage.loginButton");
    }
}
