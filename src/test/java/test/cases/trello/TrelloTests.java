package test.cases.trello;

import pages.trello.BoardPage;
import pages.trello.BoardsPage;
import pages.trello.LoginPage;


import org.junit.Test;

public class TrelloTests extends BaseTest {

    private String boardName; //Could be added as a property and make it unique. No board name uniqueness.
    private String boardId;

    @Test
    public void createBoard_When_CreateBoardClicked() {
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");

        BoardsPage boardsPage = new BoardsPage(actions.getDriver());
        //Create a board via the UI
        /*#########################*/
        boardsPage.clickOnBoard(boardName);

        BoardPage boardPage = new BoardPage(actions.getDriver());
        boardPage.assertListExists("To Do");
        //Get the board id
    }

    @Test
    public void createNewCardInExistingBoard_When_CreateCardClicked() {

    }

    @Test
    public void moveCardBetweenStates_When_DragAndDropIsUsed() {
    }

    @Test
    public void deleteBoard_When_DeleteButtonIsClicked() {
    }
}
