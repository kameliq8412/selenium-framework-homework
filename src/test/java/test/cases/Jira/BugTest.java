package test.cases.Jira;

import org.junit.Test;
import pages.jira.AllProjectsPage;
import pages.jira.CreateBugPage;

public class BugTest extends BaseTest{

    private String project;
    @Test
    public void create_Bug_Test(){

        AllProjectsPage allProjectsPage = new AllProjectsPage(actions.getDriver());
        allProjectsPage.clickOnProject(project);

        CreateBugPage createBug = new CreateBugPage(actions.getDriver());
        createBug.clickOnCreateBugButton();

        createBug.assertBugCreated();
    }
}
