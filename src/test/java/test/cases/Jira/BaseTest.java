package test.cases.Jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import pages.jira.LoginJiraPage;


public class BaseTest {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.url");
    }

    @Before
    public void logIn(){
        LoginJiraPage loginJiraPage = new LoginJiraPage(actions.getDriver());
        loginJiraPage.logInUser("user");
    }

//    @AfterClass
//    public static void tearDown() {
//        UserActions.quitDriver();
//    }
}
