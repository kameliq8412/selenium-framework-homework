package test.cases.Jira;

import org.junit.Test;
import pages.jira.AllProjectsPage;
import pages.jira.LinkStoryAndBug;

public class LinkStoryAndBugTest extends BaseTest{

    String project;

    @Test
    public void link_Story_And_Bug_Test(){

        AllProjectsPage allProjectsPage = new AllProjectsPage(actions.getDriver());
        allProjectsPage.clickOnProject(project);

        LinkStoryAndBug linkStoryAndBug = new LinkStoryAndBug(actions.getDriver());
        linkStoryAndBug.linkIssues();

        linkStoryAndBug.assertIssuesLiked();
    }
}
