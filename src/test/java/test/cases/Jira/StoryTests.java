package test.cases.Jira;
import org.junit.Test;
import pages.jira.*;


public class StoryTests extends BaseTest {
    private String project;

    @Test
    public void create_Story_Test() {

        AllProjectsPage allProjectsPage = new AllProjectsPage(actions.getDriver());
        allProjectsPage.clickOnProject(project);

        CreateStoryPage createStory = new CreateStoryPage(actions.getDriver());
        createStory.clickOnCreateIssueButton();

        createStory.assertStoryCreated();
    }
}



